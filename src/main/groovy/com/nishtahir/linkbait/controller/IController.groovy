package com.nishtahir.linkbait.controller

/**
 * Interface for controllers used in Webservice
 */
interface IController {

    /**
     * Initialize routes
     */
    void init()

}
